# AI4EO forecast

Developing an open source library to compare Earth Observation and weather forecast services with the actual measurements and assess the accuracy of the forescast

## Install
The install process is quite simple, mostly can be done by `pip`.
### Dependencies
These dependencies should be installed from your own repository or from some other source:

* [proj](https://proj.org/)
* [geos](https://trac.osgeo.org/geos/)

Before installing the `ai4eo-forecast` package with `pip`, if your `proj` version is newer you have to install [Cython](https://cython.org/) and [cartopy](https://scitools.org.uk/cartopy/docs/latest/) as it is shown below because of this  [issue](https://github.com/SciTools/cartopy/pull/1289) of `cartopy`. This is going to be fixed in its next release.

```bash
pip install Cython
pip install git+https://github.com/snowman2/cartopy.git@5e624fe
```

### Install ai4eo-forecast

You need to issue this command at the top level of the source directory:

```bash
pip install .
```

This is going to download all the necessary dependencies which was not addressed before and install `ai4eo-forecast`.


## Structure of the library

The library is separated into three different modules:
* utility
* visualization
* data_fetcher

### Utility
This module contains commonly used algorithms to make your life easier while processing and evaluating data.

### Visulaization
This module helps in visualizing and animating geographical data out of the box in a fancy way. These are especially useful to explore your data before you would like to create your own fancy figure/animation.

### DataFetcher
It is often happens that you need to grab some data from a given FTP server to be able to create your own time series. The catch is that the previous data is often removed from the server. This tool makes it possible to save every data from the given repo when it is updated and it even stores the details in a local database file.

## Under the hood
ai4eo_forecast is based on [xarray](http://xarray.pydata.org) and [dask](https://dask.org/). These libraries were used because of the provided high flexibility, scaling and rich features. Visualziation is based on [cartopy](https://scitools.org.uk/cartopy) and [matplotlib](https://matplotlib.org/).

## Tutorial
You can find a detailed tutorial under the [docs](https://gitlab.com/DeepBlueGlobe/ai4eo-forecast/blob/master/docs/AI4EO%20Tutorial.ipynb).
