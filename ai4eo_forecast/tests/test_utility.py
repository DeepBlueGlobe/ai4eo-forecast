import pytest
import xarray as xr
import numpy as np

from ..utility import find_nc_files_in_folder
from ..utility import load_data_into_list
from ..utility import calculate_msqe_between_datasets


@pytest.mark.parametrize("path,expected",
                         [("assets/tests",
                          ["assets/tests/simple_set.nc", ]),
                          ("assets/tests/load/nonc",
                           []),
                          ("assets/tests/load/abc/",
                           ["assets/tests/load/abc/a.nc",
                            "assets/tests/load/abc/b.nc",
                            "assets/tests/load/abc/c.nc"])])
def test_find_nc_files_in_folder(path, expected):
    res = find_nc_files_in_folder(path)
    assert expected == res


@pytest.mark.parametrize("data,result_names",
                         [(["assets/tests/load/abc/a.nc", ], ["a", ]),
                          (["assets/tests/load/abc/a.nc",
                            "assets/tests/load/abc/b.nc"], ["a", "b"])])
def test_load_data_into_list_v1(data, result_names):
    res = load_data_into_list(data)
    for i, j in enumerate(res):
        assert j[result_names[i]].name == result_names[i]

@pytest.mark.parametrize("data,result_names",
                         [(["assets/tests/load/abc/c.nc", ], ["c", ])])
def test_load_data_into_list_no_time(data, result_names):
    r"""
    time is a necessary dimension to be able to load data
    """
    try:
        load_data_into_list(data)
    except ValueError:
        return
    assert False


def create_dataset(num, longitude, latitude):
    time = np.arange(num)
    lon = np.arange(num*2)
    lat = np.arange(num*3)

    data = []
    for i in range(10):
        data.append(np.random.rand(num, num*2, num*3))
    datasets = []

    for i in data:
        a = xr.DataArray(i,
                         name="variable",
                         coords=[("time", time),
                                 (longitude, lon),
                                 (latitude, lat)])
        datasets.append(a)
    return (data, datasets)


def test_calculate_msqe_between_datasets():
    # Create dataset first
    data, datasets = create_dataset(30, "longitude", "latitude")

    # Select reference
    reference_np = data[-1]
    reference_ds = datasets[-1]
    data.pop()
    datasets.pop()

    def calculate_msqe_numpy(data, reference):
        mse = np.square(data - reference).mean(axis=(1, 2))
        return mse

    # First with only a simple dataset
    res = calculate_msqe_between_datasets(datasets[0],
                                          reference_ds,
                                          longitude="longitude",
                                          latitude="latitude")

    mse = calculate_msqe_numpy(data[0], reference_np)

    assert np.allclose(res.data, mse)

    # Check for list
    res = calculate_msqe_between_datasets(datasets,
                                          reference_ds,
                                          longitude="longitude",
                                          latitude="latitude")

    for i, j in enumerate(data):
        assert np.allclose(calculate_msqe_numpy(j, reference_np),
                           res[i].data)

    # Create dataset with different lat lon dims
    # Create dataset first
    data, datasets = create_dataset(30, "lon", "lat")

    # Select reference
    reference_np = data[-1]
    reference_ds = datasets[-1]
    data.pop()
    datasets.pop()

    # First with only a simple dataset
    res = calculate_msqe_between_datasets(datasets[0],
                                          reference_ds,
                                          longitude="lon",
                                          latitude="lat")

    mse = calculate_msqe_numpy(data[0], reference_np)

    assert np.allclose(res.data, mse)

    # Check for list
    res = calculate_msqe_between_datasets(datasets,
                                          reference_ds,
                                          longitude="lon",
                                          latitude="lat")

    for i, j in enumerate(data):
        assert np.allclose(calculate_msqe_numpy(j, reference_np),
                           res[i].data)
